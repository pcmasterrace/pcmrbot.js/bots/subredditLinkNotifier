module.exports = {
	apps : [{
		name      : 'bot/subredditLinkNotifier',
		script    : 'build/notifier.js',
		env: {
			// Either inject the values via environment variables or define them here
			TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
			BOT_LINKNOTIFIER_SUBREDDIT: process.env.BOT_LINKNOTIFIER_SUBREDDIT || "",
			BOT_LINKNOTIFIER_DB_DIALECT: process.env.BOT_LINKNOTIFIER_DB_DIALECT || undefined,
			BOT_LINKNOTIFIER_DB_HOST: process.env.BOT_LINKNOTIFIER_DB_HOST || undefined,
			BOT_LINKNOTIFIER_DB_NAME: process.env.BOT_LINKNOTIFIER_DB_NAME || undefined,
			BOT_LINKNOTIFIER_DB_USERNAME: process.env.BOT_LINKNOTIFIER_DB_USERNAME || undefined,
			BOT_LINKNOTIFIER_DB_PASSWORD: process.env.BOT_LINKNOTIFIER_DB_PASSWORD || undefined,
			BOT_LINKNOTIFIER_DB_PORT: process.env.BOT_LINKNOTIFIER_DB_PORT || undefined,
			BOT_LINKNOTIFIER_DB_PATH: process.env.BOT_LINKNOTIFIER_DB_PATH || undefined,
			BOT_LINKNOTIFIER_DB_DROP_TABLES_ON_START: process.env.BOT_LINKNOTIFIER_DB_DROP_TABLES_ON_START || false
		}
	}]
};
