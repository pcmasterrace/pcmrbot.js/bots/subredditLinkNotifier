import { Service, Context, Errors } from "moleculer";

class NotifierService extends Service {
	protected subreddit: string;

	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "bot.subNotifier",
			version: 2,
			dependencies: [
				{name: "reddit-rt.posts", version: 2},
				{name: "reddit.utilities", version: 1},
				{name: "slack.web", version: 1}
			],
			events: {
				"reddit-rt.comment": this.eventHandler,
				"reddit-rt.submission": this.eventHandler
			},
			created: this.serviceCreated
		});
	}

	async eventHandler(originalPost) {
		if (originalPost.subreddit != this.subreddit) return;

		
	}

	async serviceCreated() {
		this.subreddit = process.env.BOT_SUBNOTIFIER_SUBREDDIT;
		this.slackChannel = process.env.BOT_SUBNOTIFIER_CHANNEL;
		if(!this.subreddit) throw new Errors.MoleculerError("You didn't supply a subreddit for the subreddit bot, you dingus");
		if(!this.slackChannel) throw new Errors.MoleculerError("You didn't specify which Slack channel you want to log to, you dingus");
	}
}

module.exports = NotifierService;